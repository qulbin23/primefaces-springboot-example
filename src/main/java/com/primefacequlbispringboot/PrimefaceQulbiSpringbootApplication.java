package com.primefacequlbispringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrimefaceQulbiSpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrimefaceQulbiSpringbootApplication.class, args);
	}

}
